/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caculatertt;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hong
 */
public class Frame {
    
    static String clientIP = null;
    static String severIP = null;
    int no;
    double time;
    String source = null;
    String destination = null;
    
    public static void setClientSeverIP(String clientIP, String severIP){
        Frame.clientIP = clientIP;
        Frame.severIP = severIP;
    }
    
    Frame(int no, double time, String source, String destination){
        this.no = no;
        this.time = time;
        this.source = source;
        this.destination = destination;
    }
    public static void mapFrame(ArrayList<ClientToSeverFrame> cts, ArrayList<SeverToClientFrame> stc) throws Exception{
        int currHighestSequenceStc = -1;
        
        // set no of sever to client frame corespond to the client to sever frame
        int i = 0;
        for(int j = 0; j < stc.size(); j++){
            while(cts.get(i).seq + cts.get(i).len <= stc.get(j).ack){
                
                //If there is a packet that should have been sent earlier
                //it is the retransmission packet
                if (currHighestSequenceStc >= cts.get(i).seq){
                    cts.get(i).isRetransmissionTCP = true;
                }
                else currHighestSequenceStc = cts.get(i).seq;
                
                cts.get(i).noSeverToClientFrame = stc.get(j).no;
                cts.get(i).rtt = (double)Math.round((stc.get(j).time - cts.get(i).time)*1000)/1000;
                i++;
                if(i == cts.size()) break;
            }
            if(i == cts.size() && j < stc.size() - 1){
                throw new Exception("This csv file error, redundant frame from sever to client");
            }
        }
        showRTT(cts);
        showRetransmissionTCPs(cts);
    }
    
    public static void showRTT(ArrayList<ClientToSeverFrame> cts){
        System.out.println("=====RTT=====");
        for(int i = 0; i < cts.size(); i++){
            System.out.println("No.:" + cts.get(i).no + " No. Ack frame from sever: " + cts.get(i).noSeverToClientFrame + " RTT = " + cts.get(i).rtt);
        }
        System.out.println("\n");
    }
    
    // bonus
    public static void showRetransmissionTCPs(ArrayList<ClientToSeverFrame> cts){
        System.out.println("=====RetransmissionTCP=====");
        boolean noRetransmission = true;
        for(int i = 0; i < cts.size(); i++){
            if (!cts.get(i).isRetransmissionTCP)
                continue;
            System.out.println("No.:" + cts.get(i).no + " is retransmission TCP packet");
            noRetransmission = false;
        }
        if(noRetransmission)  System.out.println("No retransmission!");
        System.out.println("Done");
    }
    
    public void showAtribute(){
        System.out.println("No.: " + no);
        System.out.println("Time: " + time);
        System.out.println("Source: " + source);
        System.out.println("Dest: " + destination);
    }
}
