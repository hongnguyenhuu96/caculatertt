/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caculatertt;

/**
 *
 * @author Hong
 */
public class ClientToSeverFrame extends Frame{
    int seq;
    int len;
    int noSeverToClientFrame;
    double rtt;
    boolean isRetransmissionTCP;
    
    public ClientToSeverFrame(int no, double time, String source, String destination, int seq, int len) {
        super(no, time, source, destination);
        this.seq = seq;
        this.len = len;
        noSeverToClientFrame = -1;
        rtt = -1;
        isRetransmissionTCP = false;
    }
    public void showAtribute(){
        super.showAtribute();
        System.out.println("seq: " + seq);
        System.out.println("len: " + len);
        System.out.println("rtt: " + rtt);
        System.out.println("noSTC: " + noSeverToClientFrame);
        
    }
}
