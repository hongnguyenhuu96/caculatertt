/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caculatertt;

import java.util.ArrayList;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;

/**
 *
 * @author Hong
 */
public class CaculateRTT extends Application {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        readFile("src\\caculatertt\\nn2.csv");
        Frame.mapFrame(cts, stc);
        launch(args);
    }

    public static void readFile(String fileName) throws java.io.IOException {
// TODO Auto-generated method stub
        java.io.FileInputStream file;
        java.io.InputStreamReader isr;
        java.io.BufferedReader rd;
        String line;
        String[] tokens = null;
        file = new java.io.FileInputStream(fileName);
        isr = new java.io.InputStreamReader(file);
        rd = new java.io.BufferedReader(isr);
        line = rd.readLine();
        while (line != null) {
            tokens = line.split(",", 7); // Tách chuỗi line thành nhiều mảnh
            manipulateTokens(tokens);
            if (tokens[INFO].contains("[SYN]")) {
                // set clientIP and sever IP
                Frame.setClientSeverIP(tokens[SOURCE], tokens[DEST]);
            } else if (tokens[INFO].contains("[SYN, ACK]") || tokens[INFO].equals("Info")) {
                // ignore three handshake and the first line
            } else {
                creatFrame(tokens);
            }
            line = rd.readLine();
        }
        file.close();
    }

    public static void show(String[] tokens) {
        for (int i = 0; i < tokens.length; i++) {
            System.out.print(tokens[i] + " ");
        }
        System.out.println("");
    }

    public static void manipulateTokens(String[] tokens) {
        for (int i = 0; i < tokens.length; i++) {
            tokens[i] = tokens[i].substring(1, tokens[i].length() - 1);
        }
    }

    public static void creatFrame(String[] tokens) {
        int no = Integer.parseInt(tokens[NO]);
        double time = Double.parseDouble(tokens[TIME]);
        if (tokens[SOURCE].equals(Frame.clientIP) && tokens[DEST].equals(Frame.severIP) && tokens[PROTOCOL].equals("TCP")) {
            int seq = Integer.parseInt(tokens[INFO].substring(tokens[INFO].indexOf("Seq") + 4, tokens[INFO].indexOf(" ", tokens[INFO].indexOf("Seq"))));
            int len = Integer.parseInt(tokens[INFO].substring(tokens[INFO].indexOf("Len") + 4));
            if (len != 0) {
                cts.add(new ClientToSeverFrame(no, time, tokens[SOURCE], tokens[DEST], seq, len));
            }
        }
        if (tokens[SOURCE].equals(Frame.severIP) && tokens[DEST].equals(Frame.clientIP) && tokens[PROTOCOL].equals("TCP")) {
            int ack = Integer.parseInt(tokens[INFO].substring(tokens[INFO].indexOf("Ack") + 4, tokens[INFO].indexOf(" ", tokens[INFO].indexOf("Ack"))));
            int len = Integer.parseInt(tokens[INFO].substring(tokens[INFO].indexOf("Len") + 4));
            if (len == 0) {
                stc.add(new SeverToClientFrame(no, time, tokens[SOURCE], tokens[DEST], ack));
            }
        }
    }

    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("Round Trip Time");
        //defining the axes
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel("Sequence Number");
        yAxis.setLabel("Round Trip Time(ms)");
        //creating the chart
        final LineChart<Number, Number> lineChart
                = new LineChart<Number, Number>(xAxis, yAxis);
        lineChart.setTitle("Round Trip Time");
        //defining a series
        XYChart.Series series = new XYChart.Series();
        series.setName(Frame.clientIP + " - " + Frame.severIP);
        //populating the series with data

        for (int i = 0; i < CaculateRTT.cts.size(); i++) {
            series.getData().add(new XYChart.Data<>(CaculateRTT.cts.get(i).seq, CaculateRTT.cts.get(i).rtt));
        }
        Scene scene = new Scene(lineChart, 800, 600);
        lineChart.getData().add(series);
        stage.setScene(scene);
        stage.show();
    }

    static final int NO = 0;
    static final int TIME = 1;
    static final int SOURCE = 2;
    static final int DEST = 3;
    static final int INFO = 6;
    static final int PROTOCOL = 4;
    static ArrayList<ClientToSeverFrame> cts = new ArrayList<>();
    static ArrayList<SeverToClientFrame> stc = new ArrayList<>();

}
